from py2neo import Graph, NodeMatcher, data, RelationshipMatcher
from db_store import PersonRel, BlogRel, JobRel, SystemStore

graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))

nodeMatcher = NodeMatcher(graph)
relationshipMatcher = RelationshipMatcher(graph)


def initAllBlog():
    print("init blog...")
    result: [data.Node] = nodeMatcher.match("Blog").all()
    allBlog: {str: BlogRel} = {}
    for item in result:
        allBlog[item.identity] = BlogRel(item.identity)
    SystemStore.blogRelDic = allBlog


def initAllPerson():
    print("init person...")
    result: [data.Node] = nodeMatcher.match("Person").all()
    allPerson: {str: PersonRel} = {}
    for item in result:
        allPerson[item.identity] = PersonRel(item.identity)
    SystemStore.personRelDic = allPerson


def initAllJob():
    print("init job...")
    result: [data.Node] = nodeMatcher.match("Job").all()
    allJob: {str: JobRel} = {}
    for item in result:
        allJob[item.identity] = JobRel(item.identity)
    SystemStore.jobRelDic = allJob


def buildPersonRel():
    print("init rel...")
    for personRel in SystemStore.personRelDic.values():
        allPersonRel = graph.run(f'MATCH (p:Person)-[r]->(t) WHERE id(p)={personRel.id} RETURN type(r), id(t)').data()
        for rel in allPersonRel:
            _type = rel["type(r)"]
            _id = int(rel["id(t)"])
            if _type == "Do":
                personRel.doJob[_id] = SystemStore.jobRelDic[_id]
                SystemStore.jobRelDic[_id].beDo[personRel.id] = personRel
            elif _type == "Write":
                personRel.writeBlog[_id] = SystemStore.blogRelDic[_id]
                SystemStore.blogRelDic[_id].beWrote = personRel
            elif _type == "Like":
                personRel.likeBlog[_id] = SystemStore.blogRelDic[_id]
                SystemStore.blogRelDic[_id].beLiked.append(personRel)


def getLikeBlogsByPersonId(id):
    data = graph.run(f'MATCH (p:Person)-[r:Like]->(b:Blog) WHERE id(p)={id} RETURN id(b)').data()
    result = [item["id(b)"] for item in data]
    return result
