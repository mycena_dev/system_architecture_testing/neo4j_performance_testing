from flask import Flask, jsonify
from db_store import SystemStore, PersonRel
import db_service as dbs
import time

st = time.time()
dbs.initAllPerson()
print("init person (40,000) cost time : " + str(time.time() - st))
st = time.time()
dbs.initAllBlog()
print("init blog (100,000) cost time : " + str(time.time() - st))
st = time.time()
dbs.initAllJob()
print("init job (311) cost time : " + str(time.time() - st))
st = time.time()
dbs.buildPersonRel()
print("init all rel (2,135,986) cost time : " + str(time.time() - st))

app = Flask(__name__)


@app.route("/likeBlogsFromDB/<personId>")
def getLikeBlogsFromDB(personId: int):
    return jsonify(result=dbs.getLikeBlogsByPersonId(personId))


@app.route("/likeBlogsFromStore/<personId>")
def getLikeBlogsFromStore(personId: int):
    result: PersonRel = SystemStore.personRelDic[int(personId)]
    return jsonify(result=list(result.likeBlog.keys()))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6060)
