from neo4j import GraphDatabase
from py2neo import Graph
from faker import Faker
import random
import time
import threading

fake = Faker('zh_TW')

Num = 5000
def insertData(fakeDataNum, index):
    pre_index = index * fakeDataNum
    # insert data by several transaction
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
    startTime = time.time()
    for a in range(fakeDataNum):
        i = a + pre_index
        tx = graph.begin()
        # print(i)
        title: str = fake.paragraph()
        content: str = fake.text()
        tx.run("CREATE (blog:Blog {id: $id ,title: $title, content: $content})", title=title, content=content, id=i)
        tx.commit()
    print("Total cost time: " + str(time.time() - startTime))

for i in range(20):
    threading.Thread(target=insertData, args=(Num, i,)).start()