from neo4j import GraphDatabase
from py2neo import Graph, NodeMatcher, Relationship
from faker import Faker
import random
import time
import threading

graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))

nodeMatcher = NodeMatcher(graph)

personNum = nodeMatcher.match("Person").count()
blogNum = nodeMatcher.match("Blog").count()


def getPersonIDS():
    tx = graph.begin()
    result = tx.run("MATCH (n:Person) RETURN id(n)").data()
    idList = [ data['id(n)'] for data in result]
    return idList

def getBlogIDS():
    tx = graph.begin()
    result = tx.run("MATCH (n:Blog) RETURN id(n)").data()
    idList = [ data['id(n)'] for data in result]
    return idList

def insertData(needNum, index):
    pre_index = index * needNum
    global  global_pidList, global_bidList
    # insert data by several transaction
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
    st = time.time()
    for a in range(needNum):
        i = a + pre_index
        bid = global_bidList[i]
        blog = nodeMatcher.get(bid)
        pid = random.randint(0, personNum - 1)
        person = nodeMatcher.get(global_pidList[pid])
        r = Relationship(person, "Write", blog)
        graph.create(r)
    print("Total time: " + str(time.time() - st))

global_pidList = getPersonIDS()
global_bidList = getBlogIDS()
threadNum = 16
_needNum = int(len(global_bidList)/threadNum)
print("All Blogs: ", str(len(global_bidList)))
print("Each thread need to process num: ", str(_needNum))
for i in range(threadNum):
    threading.Thread(target=insertData, args=(_needNum, i)).start()
