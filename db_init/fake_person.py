from neo4j import GraphDatabase
from py2neo import Graph
from faker import Faker
import random
import time
import threading

fake = Faker('zh_TW')

Num = 5000
def insertData(fakeDataNum, index):
    # insert data by several transaction
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
    startTime = time.time()
    for a in range(fakeDataNum):
        i = a + index
        tx = graph.begin()
        # print(i)
        phoneNumber: str = fake.phone_number()
        address: str = fake.address()
        age: int = random.randint(12, 90)
        email: str = fake.ascii_free_email()
        gender: str = 'male' if random.randint(0,3) > 1  else 'female'
        _firstName = fake.first_name_male() if gender is 'male' else fake.first_name_female()
        name:str = fake.last_name() + _firstName
        tx.run("CREATE (person:Person {id: $id ,name: $name, address: $address, age: $age, email: $email, gender: $gender})", name=name, address=address, age=age, email=email, gender=gender, id=i)
        tx.commit()
    print("Total cost time: " + str(time.time() - startTime))
    startTime = time.time()
    # print(tx.commit())
    print("Total cost time: " + str(time.time() - startTime))

t1 = threading.Thread(target=insertData, args=(Num, Num*0,))
t2 = threading.Thread(target=insertData, args=(Num, Num*1,))
t3 = threading.Thread(target=insertData, args=(Num, Num*2,))
t4 = threading.Thread(target=insertData, args=(Num, Num*3,))
t5 = threading.Thread(target=insertData, args=(Num, Num*4,))
t6 = threading.Thread(target=insertData, args=(Num, Num*5,))
t7 = threading.Thread(target=insertData, args=(Num, Num*6,))
t8 = threading.Thread(target=insertData, args=(Num, Num*7,))
t1.start()
t2.start()
t3.start()
t4.start()
t5.start()
t6.start()
t7.start()
t8.start()
# insert data by one transaction
# graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
# tx = graph.begin()
# startTime = time.time()
# queryStr = ''
# for i in range(fakeDataNum):
#     print(i)
#     phoneNumber: str = fake.phone_number()
#     address: str = fake.address()
#     age: int = random.randint(12, 90)
#     email: str = fake.ascii_free_email()
#     gender: str = 'male' if random.randint(0,3) > 1  else 'female'
#     _firstName = fake.first_name_male() if gender is 'male' else fake.first_name_female()
#     name:str = fake.last_name() + _firstName
#     queryStr += "CREATE ( P_" +str(i)+ ":Person {id: "+str(i)+" ,name: \"" +name+ "\" , address: \""+address+"\" , age: " + str(age) + ", email: \"" + email + "\" , gender: \"" +gender+ "\"}) \n"
#
# tx.run(queryStr)
# print("Total cost time: " + str(time.time() - startTime))
# startTime = time.time()
# print(tx.commit())
# print("Total cost time: " + str(time.time() - startTime))