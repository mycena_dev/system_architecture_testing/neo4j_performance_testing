from neo4j import GraphDatabase
from py2neo import Graph, NodeMatcher, Relationship
from faker import Faker
import random
import time
import threading

graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))

nodeMatcher = NodeMatcher(graph)

personNum = nodeMatcher.match("Person").count()
jobNum = nodeMatcher.match("Job").count()


def getPersonIDS():
    tx = graph.begin()
    result = tx.run("MATCH (n:Person) RETURN id(n)").data()
    idList = [ data['id(n)'] for data in result]
    return idList

def getJobIDS():
    tx = graph.begin()
    result = tx.run("MATCH (n:Job) RETURN id(n)").data()
    idList = [ data['id(n)'] for data in result]
    return idList

def insertData(needNum, index):
    pre_index = index * needNum
    global  global_pidList, global_jidList, jobNum
    # insert data by several transaction
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
    st = time.time()
    for a in range(needNum):
        i = a + pre_index
        pid = global_pidList[i]
        person = nodeMatcher.get(pid)
        jid = random.randint(0, jobNum - 1)
        job = nodeMatcher.get(global_jidList[jid])
        r = Relationship(person, "Do", job)
        graph.create(r)
    print("Total time: " + str(time.time() - st))

global_pidList = getPersonIDS()
global_jidList = getJobIDS()
threadNum = 8
_needNum = int(len(global_pidList)/threadNum)
print("All person: ", str(len(global_pidList)))
print("need num: ", str(_needNum))
for i in range(threadNum):
    threading.Thread(target=insertData, args=(_needNum, i)).start()
