from neo4j import GraphDatabase
from py2neo import Graph
from faker import Faker
import random
import time
import threading

fake = Faker('zh_TW')

Num = 500
def insertData(jobList):
    # insert data by several transaction
    graph = Graph("bolt://localhost:7687", auth=("neo4j", "hn74121886"))
    startTime = time.time()
    for a in range(len(jobList)):
        i = a
        tx = graph.begin()
        name = jobList[i]
        tx.run("CREATE (job:Job {id: $id ,name: $name})", name=name, id=i)
        tx.commit()
    print("Total cost time: " + str(time.time() - startTime))
jobList = []
for i in range(Num):
    jobList.append(fake.job())

insertData(list(set(jobList)))