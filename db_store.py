class BlogRel:
    def __init__(self, id):
        self.id = id
        self.beLiked: [PersonRel] = []
        self.beWrote: PersonRel = None


class JobRel:
    def __init__(self, id):
        self.id = id
        self.beDo: {int: PersonRel} = {}


class PersonRel:
    def __init__(self, id):
        self.id = id
        self.likeBlog: {int: BlogRel} = {}
        self.writeBlog: {int: BlogRel} = {}
        self.doJob: {int: JobRel} = {}


class SystemStore:
    personRelDic: {int: PersonRel} = {}
    blogRelDic: {int: BlogRel} = {}
    jobRelDic: {int: JobRel} = {}